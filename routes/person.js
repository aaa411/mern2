const express = require('express')

const db = require('../db')

const utils = require('../utils')

const encry = require('crypto-js')

const router = express.Router();

router.get('/', (req,resp) =>{
     const query = `select id,name,address from SDM`;
     db.pool.execute(query,(error,persons) =>{

        resp.send(utils.createResult(error,persons))
     })
})

router.post('/', (req,resp) =>{
    const {name ,address,password} = req.body;

    // encryt the password
    const encrypassword = encry.MD5(password)
    const query = `insert into SDM (name,address,password) values(?,?,?)`;
    db.pool.execute(query,[name,address,encrypassword], (error,result) =>{
        resp.send(utils.createResult(error,result))
    })

})

module.exports = router
